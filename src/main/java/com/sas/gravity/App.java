/*
Copyright (c) 2015, Imre Városi
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 43):
 * <sas972@gmail.com> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Imre Városi
 * ----------------------------------------------------------------------------
 */

package com.sas.gravity;

import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

public class App extends Frame implements Runnable
{
	World world = new World();
	int div =2;
	List<Vector2D> lastPainted = new ArrayList<Vector2D>();
	public void run() {
		for(;;){
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			repaint();
		}
	}
	public App(){
		super("Gravity");
		
		setSize(400,400);
	      addWindowListener(new WindowAdapter(){
	         public void windowClosing(WindowEvent windowEvent){
	            System.exit(0);
	         }        
	      }); 
	    addKeyListener(new KeyListener() {
			
			public void keyTyped(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_PLUS){
		            div++;
		            System.out.println("++++");
		            return;
		         }
				if(e.getKeyCode() == KeyEvent.VK_MINUS){
		            if(1<div)
		            	div--;
		            System.out.println("++++");
		            return;
		         }
				
			}
			
			public void keyReleased(KeyEvent arg0) {
				
			}
			
			public void keyPressed(KeyEvent arg0) {
				
			}
		});
	    
	    addMouseWheelListener(new MouseWheelListener() {
			
			public void mouseWheelMoved(MouseWheelEvent e) {
				if(0 < e.getWheelRotation()){if(1<div)div--;}else div++;
				
			}
		});
	}
	
	@Override
	public void paint(Graphics g) {
      Graphics2D g2 = (Graphics2D)g;
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	  RenderingHints.VALUE_ANTIALIAS_ON);
	  Font font = new Font("Serif", Font.PLAIN, 24);
	  g2.setFont(font);
	  g2.setColor(Color.BLACK);
	  g2.drawString("Div: " +div, 50, 70); 
	  g2.setColor(Color.WHITE);
	  for (Vector2D pos: lastPainted){
		  g2.fillOval((int)pos.x/div, (int)pos.y/div, 12, 12);
	  }
	  lastPainted.clear();
	  g2.setColor(Color.BLACK);
	  for(Vector2D pos: world.calculateNext()){
		  g2.fillOval((int)pos.x/div, (int)pos.y/div, 10, 10);
		  lastPainted.add(pos);
	  }
   } 
	
    public static void main( String[] args )
    {
        App app =new App();
        app.setVisible(true);
        new Thread(app).start();
        System.out.println( "End" );
    }
    
    public void update(Graphics g){
    	paint(g);
    }
}
