/*
Copyright (c) 2015, Imre Városi
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/



/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <sas972@gmail.com> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Imre Városi
 * ----------------------------------------------------------------------------
 */

package com.sas.gravity;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

import java.awt.geom.Point2D.Double;

public class Vector2D extends Double {

	private static final long serialVersionUID = 6042638974437937979L;

	public Vector2D() {
		super(0d, 0d);
	}

	public Vector2D(double x, double y) {
		super(x, y);
	}

	public Vector2D add(Vector2D vectorToAdd) {
		x += vectorToAdd.x;
		y += vectorToAdd.y;
		return this;
	}

	public Vector2D sub(Vector2D vector2) {
		x -= vector2.x;
		y -= vector2.y;
		return this;
	}

	public Vector2D multiply(double d) {
		x = x * d;
		y = y * d;
		return this;
	}
	public Vector2D div(double d) {
		x = x / d;
		y = y / d;
		return this;
	}

	public double abs2() {
		return pow(x, 2) + pow(y, 2);
	}

	public double abs() {
		return sqrt(abs2());
	}

	public String toString() {
		return "[" + x + ", " + y + "]";
	}

	public Vector2D normalize() {
		double abs = abs();
		if(0 != abs )
			return new Vector2D(x/abs, y/abs);
		return new Vector2D(0d, 0d);
	}
	
	public Object clone() {
		return new Vector2D(x, y);
	}
}
