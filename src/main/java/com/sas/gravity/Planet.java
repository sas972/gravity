
/*
Copyright (c) 2015, Imre Városi
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 43):
 * <sas972@gmail.com> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Városi Imre
 * ----------------------------------------------------------------------------
 */


package com.sas.gravity;

public class Planet {

	Vector2D pos = new Vector2D(), v = new Vector2D(), a = new Vector2D(), f = new Vector2D();
	double m;
	String id;

	public Planet(String id){
		this.id = id;
	}
	public double getM() {
		return m;
	}

	public Planet setM(double m) {
		this.m = m;
		return this;
	}

	public Vector2D getPos() {
		return pos;
	}

	public Planet setPos(Vector2D pos) {
		this.pos = pos;
		return this;
	}

	public Vector2D getV() {
		return v;
	}

	public Planet setV(Vector2D v) {
		this.v = v;
		return this;
	}

	public Vector2D getA() {
		return a;
	}

	public Planet setA(Vector2D a) {
		this.a = a;
		return this;
	}

	public Vector2D getF() {
		return f;
	}

	public Planet setF(Vector2D f) {
		this.f = f;
		return this;
	}
	
	public Planet addGForce(Planet other) {
		if(this == other) return this;
		Vector2D diff = ((Vector2D)other.getPos().clone()).sub(pos);
		Vector2D tmp = diff.normalize().multiply(m).multiply(other.m).div(diff.abs2());
		f.add(tmp);
		return this;
	}
	
	public Planet addV(Vector2D v) {
		this.v.add(v);
		return this;
	}
	
	public Planet addPos(Vector2D pos) {
		this.pos.add(pos);
		return this;
	}
	
	public Planet calculape() {
		CalcNextStrategy strategy = new SimpleStrategy();
		return strategy.calcNext(this, 0);
	}
}
