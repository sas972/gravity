/*
Copyright (c) 2015, Imre Városi
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 43):
 * <sas972@gmail.com> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Imre Városi
 * ----------------------------------------------------------------------------
 */
package com.sas.gravity;

import java.util.ArrayList;
import java.util.List;

public class World {

	
	List<Planet> planets = new ArrayList<Planet>();
	public World(){
		planets.add(new Planet("nap").setM(10000d).setPos(new Vector2D(1000,1000)));
		planets.add(new Planet("p1").setM(10d).setPos(new Vector2D(700,700)).setV(new Vector2D(0,3)));
		planets.add(new Planet("p2").setM(25d).setPos(new Vector2D(1300,700)).setV(new Vector2D(-5,0)));
	}
	
	public List<Vector2D> calculateNext(){
		List<Vector2D> ret = new ArrayList<Vector2D>();
		for( Planet p: planets){
			p.setF(new Vector2D());
			for(Planet p2 : planets){
				p.addGForce(p2);
			}
			p.calculape();
			ret.add(p.getPos());
		}
		return ret;
	}
}
