package com.sas.gravity;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.*;

import org.hamcrest.core.IsEqual;
import org.hamcrest.core.IsSame;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.sas.gravity.Vector2D;

public class Vector2DTest {

	private Vector2D vector;

	@Before
	public void setUp() throws Exception {
		vector = new Vector2D(2d,3d);
	}

	@After
	public void tearDown() throws Exception {
	}


	@Test
	public void testVector2DConstructor() {
		assertEquals(2d, vector.getX(),.01d);
		assertEquals(3d, vector.getY(),.01d);
	}
	
	@Test
	public void testAdd() throws Exception {
		Vector2D vectorToAdd = new Vector2D(1d,5d);
		Vector2D tmp = vector.add(vectorToAdd);
		assertEquals(3d, vector.getX(),.01d);
		assertEquals(8d, vector.getY(),.01d);
		assertThat(tmp, sameInstance(vector));
	}
	
	@Test
	public void testSub() throws Exception {
		Vector2D vector2 = new Vector2D(1d,5d);
		Vector2D tmp = vector.sub(vector2);
		assertEquals(1d, vector.getX(),.01d);
		assertEquals(-2d, vector.getY(),.01d);
		assertThat(tmp, sameInstance(vector));
	}
	
	@Test
	public void testMultiply() throws Exception {
		Vector2D tmp = vector.multiply(2d);
		assertEquals(4d, vector.getX(),.01d);
		assertEquals(6d, vector.getY(),.01d);
		assertThat(tmp, sameInstance(vector));
	}
	
	@Test
	public void testAbs2() throws Exception {
		assertEquals(13d, vector.abs2(),.01d);
	}
	
	@Test
	public void testAbs() throws Exception {
		vector = new Vector2D(3d, 4d);
		assertEquals(5d, vector.abs(),.01d);
	}
	
	@Test
	public void testToString() throws Exception {
		assertThat("[2.0, 3.0]".equals(vector.toString()), is(true));
	}
	
	@Test
	public void testNormalize() throws Exception {
		vector = new Vector2D(3d, 4d);
		vector = vector.normalize();
		assertEquals(.6d, vector.x,.01d);
		assertEquals(.8d, vector.y,.01d);
		assertEquals(1d, vector.abs(),.01d);
	}
	@Test
	public void testNormalize0() throws Exception {
		vector = new Vector2D(0d, 0d);
		vector = vector.normalize();
		assertEquals(0d, vector.x,.01d);
		assertEquals(0d, vector.y,.01d);
	}
	
	
}
